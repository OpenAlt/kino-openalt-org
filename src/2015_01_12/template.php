<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="openalt">
  <link rel="icon" href="../favicon.ico" type="image/x-icon" />
  <link rel="icon" href="../favicon.png" type="image/png" />

  <title>OpenAlt <?php echo isset($title) ? "- " . $title : ""; ?></title>

  <!-- Bootstrap core CSS -->
  <link href="css/lightbox.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/leaflet.css" rel="stylesheet">
  <link href="css/timeline.css" rel="stylesheet">

  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/lightbox.min.js"></script>

  <!-- Matomo -->
  <script>
    var _paq = window._paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="https://analytics.openalt.org/";
      _paq.push(['setTrackerUrl', u+'matomo.php']);
      _paq.push(['setSiteId', '4']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Matomo Code -->
</head>

  <body>
    <span id="home"></span>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#home"> OpenAlt </a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="#schedule1"> Film </a></li>
              <li><a href="#schedule2"> Panelová diskuse </a></li>
              <li><a href="#gallery"> Fotky </a></li>
              <li><a href="#venue"> Místo konání </a></li>
              <li><a href="#contact"> Kontakt </a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li> <a href="https://www.facebook.com/OpenAlt"> <i class="fa fa-facebook"></i> </a> </li>
              <li> <a href="https://twitter.com/OpenAlt"> <i class="fa fa-twitter"></i> </a> </li>
              <li> <a href="https://plus.google.com/+OpenAlt/posts"> <i class="fa fa-google-plus"></i> </a> </li>
              <li> <a href="https://www.linkedin.com/groups?home=&amp;gid=3707492&amp;trk=anet_ug_hm"> <i class="fa fa-linkedin"></i> </a> </li>
            </ul>
          </div>
        </div>
      </div> <!--/.container -->
    </div> <!--/.navbar -->

<!--	<script type="text/javascript">
  jQuery.ajaxSetup({ cache: true });

  jQuery.getScript("https://static.jappix.com/server/get.php?l=cs&t=js&g=mini.xml", function() {
     JappixMini.launch({
        connection: {
           domain: "anonymous.jappix.com",
        },

        application: {
           network: {
              autoconnect: true,
           },

           interface: {
              showpane: true,
              animate: true,
           },

           user: {
              random_nickname: true,
           },

           groupchat: {
              open: ["conf@openalt.org"],
           },
        },
     });
  });
</script>  -->

  <?php echo $content ?? ''; ?>

  <footer class="container">
    Obsah je dostupný pod <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.cs" target="_blank">licencí Creative Commons Uveďte původ - Zachovejte licenci 4.0 Mezinárodní</a>.
    <br>
    <a href="https://www.openalt.org/kontakt" target="_blank">© OpenAlt z. s.</a>
  </footer>

  <!-- Placed at the end of the document so the pages load faster -->
  <script src="./js/bootstrap.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

  <script>
    var $root = $('html, body');
    $('a').click(function() {
        $root.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
        return false;
    });
  </script>

</body>
</html>
