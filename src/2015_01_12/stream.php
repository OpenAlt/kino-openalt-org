<?php
header('location: https://kino.openalt.org/2015_01_12/#schedule2');
exit();

require(dirname(__FILE__).'/init.php');

$title = "Sledujte žive přes internet";

$content .= <<<EOF
<section class="section-normal">

    <div class="container" id="home">

<h1> Záznam </h1>

Z panelové diskuze byl pořízen záznam, který je k dispozici zde:
<ul>
  <li> <a href="./20150112-recording-panel_discussion.mp4"/>mp4</a> (2 162 MB) </li>
  <li> <a href="./20150112-recording-panel_discussion.webm"/>webm</a> (287 MB) </li>
</ul>
<video style="width: 800px; height: 450px; display: block; margin: 0 auto;" controls>
  <source src="./20150112-recording-panel_discussion.mp4" type="video/mp4" />
  <source src="./20150112-recording-panel_discussion.webm"  type="video/webm" />
  <source src="./20150112-recording-panel_discussion.ogg"  type="video/ogg" />
</video>

<br/> <br/>

<h1> Živé vysílání </h1>

Panelová diskuze byla vysílaná živě na adrese <a href="http://stream.openalt.org:8090/discussion">http://stream.openalt.org:8090/discussion</a>.

<br/> <br/>
Pro připojení doporučujeme použít následující postup:
<ul>
  <li> nainstalovat si přehrávač <a href="http://www.videolan.org/vlc/">VLC media player</a> </li>
  <li> v němž zvolíme "Média" </li>
  <li> dále položku "Otevřít síťový proud" </li>
  <li> do kolonky vyplníme uvedenou adresu </li>
  <li> pak již stačí kliknout na "Přehrát" </li>
</ul>

    
    </div>

</section>
EOF;

require(dirname(__FILE__).'/template.php');
?>
