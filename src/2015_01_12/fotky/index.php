<?php

$title = "fotky";

$images_array = array();
$dir = dirname(__FILE__).'/big';
if ($handle = opendir($dir)) {

    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
      $fullname = "$dir/$entry";
      if (is_dir($fullname)) {
         continue;
      }


      array_push($images_array, $entry);

    }

    closedir($handle);
}

sort($images_array);

$content = <<<EOF

EOF;
foreach ($images_array as $entry) {
$content .= <<<EOF
<a href="big/$entry" data-lightbox="images" data-title="$entry"><img src="thumb/$entry"></a> 
EOF;
}
$content .= <<<EOF


EOF;

require(dirname(__FILE__).'/template.php');

?>
