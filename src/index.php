<?php

//header('location: ./2015_01_12/'); exit();

$content =<<<EOF

<h1>
<img src="./2015_01_12/img/logo.svg" style="width: 80px;" alt="OpenAlt sraz">
kino.OpenAlt.org
</h1>

<br/> <br/> <br/>


<div class="card" style="height: 180px; width: 38rem; border: solid #eee 1px; padding: 10px; margin: 10px; float: left;">
  <div class="card-body">
    <h3 class="card-title">Noc otevřeného softwaru v umění </h3>
    <p class="card-text">
        7. listopadu 2016 v 20:30 — Kino Scala
    </p>
    <a href="./2016_11_07/" class="btn btn-primary">Zobrazit</a>
  </div>
</div>


<div class="card" style="height: 180px; width: 38rem; border: solid #eee 1px; padding: 10px; margin: 10px; float: left;">
  <div class="card-body">
    <h3 class="card-title">Internetový chlapec: Příběh Aarona Swartze </h3>
    <p class="card-text">
        12. ledna 2015 - Premiéra filmu, Kino Scala
    </p>
    <a href="./2015_01_12/" class="btn btn-primary">Zobrazit</a>
  </div>
</div>

EOF;

require(dirname(__FILE__).'/template.php');
?>
