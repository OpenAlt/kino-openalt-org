<?php 
  require(dirname(__FILE__).'/init.php');

$title = "Noc otevřeného softwaru v umění";
$content .= <<<EOF

    <div class="container">
      <div class="starter-template">
      <h1>
        <img src="./img/logo.svg" style="width: 100px;" alt="OpenAlt sraz" />
        kino.OpenAlt.org
      </h1>
<br/>
<p class="lead">
EOF;
$content .= "<small>".$config['datum_misto']."</small>";

$content .= <<<EOF
</p>
      </div>
    </div><!-- /.container -->


<section class="section-normal img-responsive" id="schedule1" style="color: #ffffff; text-shadow:1px 1px 4px #000000,-1px -1px 4px #000000;  background-image:url('./img/nosvu.jpg');      background-repeat: no-repeat; background-color: #060606; a, a:link, a:visited, a:hover, a:active { color: rgba(90, 247, 235, 1); };" data-img-width="1770" data-img-height="632">

  <div class="container" style="min-height: 350px;">
    <h1> Noc otevřeného softwaru v umění  </h1>

Umělci, filozofové, architekti, designéři, spisovatelé, studenti, učitelé nebo profesionálové! Všichni, kteří chcete zjistit více o možnostech tvorby a publikování vašich prací tak, aby byly maximálně přínosné. <br/><br/>

Přijďte na Noc otevřeného softwaru v umění, kde vystoupí místní umělci a open source nadšenci a promluví o tom jak otevřený software a svobodné licence jako Art Libre, Creatice Commons a GPL nadobro mění způsob tvorby a distribuci umění i softwaru. <br/><br/>

Program bude mimo jiné obsahovat i promítání nejnovějších svobodných filmů, animací/animáků a svobodné hudby. Dále budou součástí večera přednášky o svobodném umění, softwaru a kultuře obecně. Také se společně pobavíme o důležitých tématech na následné recepci. <br/><br/>

Vezměte s sebou i kamarády a pomozte nám podpořit svobodné umění v České Republice! <br/> <br/>

<a href="https://www.facebook.com/events/1773367172928012/"><img src="./img/icons/facebook.png" class="img-rounded" alt="facebook" /></a>





  </div>

</section>

<section class="section-normal section-link" id="reg">
  <div class="container">
    <h1> Registrace </h1>
<div style="width:100%; text-align:left;"><iframe src="//eventbrite.com/tickets-external?eid=27253403664&ref=etckt" frameborder="0" height="300" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="auto" allowtransparency="true"></iframe><div style="font-family:Helvetica, Arial; font-size:12px; padding:10px 0 5px; margin:2px; width:100%; text-align:left;" ><a class="powered-by-eb" style="color: #ADB0B6; text-decoration: none;" target="_blank" href="http://www.eventbrite.com/">Powered by Eventbrite</a></div></div>

  </div>
</section>

<!-- section class="section-normal section-link" id="schedule2">
  <div class="container">

    <h1>Panelová diskuse</h1>



    <div class="img-thumbnail medialonek">
      <div class="medialonek_name">Richard Ďurana</div>
      <img src="./img/durena.jpg" class="medialonek_photo"/>
      <div class="medialonek_cv">
Richard je riaditeľom INESS - Inštitútu ekonomických a spoločenských analýz, najvplyvnejšieho a najcitovanejšieho slovenského ekonomického think tanku.
Venuje sa oblasti verejných financií, hospodárskej politiky, trhu práce a vlastníckych práv.
Je spoluautorom viacerých odborných publikácií a často komentuje hospodárske dianie v hlavných mienkotvorných médiách a na medzinárodných a domácich fórach.
Je absolventom Prírodovedeckej fakulty UK v Bratislave, doktorát získal v oblasti biochémie na Slovenskej akadémii vied, kde tiež pôsobil ako vedecký pracovník na vývoji subcelulárnych vakcín. 
Od roku 2006 pracuje v INESS.
      </div>
    </div>


    <div class="img-thumbnail medialonek">
      <div class="medialonek_name">Marcel Kolaja</div>
      <img src="./img/kolaja.jpg" class="medialonek_photo"/>
    
      <div class="medialonek_cv">
      Marcel je odborníkem na svobodný software a svobodnou kulturu. V roce 2014
byl ve volbách do Evropského parlamentu dvojkou kandidátky Pirátské
strany, jejíž je členem a kde v minulosti působil mimo jiné jako
místopředseda a mezinárodní koordinátor. Byl spolupředsedou Pirate Parties
International. V Pirátské straně inicioval projekt proti výběrčím
organizacím OSA/Intergram "Hrajeme svobodnou hudbu!" a na místní
i mezinárodní úrovni se věnuje činnostem, jejichž dlouhodobým cílem je
reforma kopírovacího monopolu (copyrightu).
      </div>
    </div>



    <div class="img-thumbnail medialonek">
      <div class="medialonek_name">Lucie Straková</div>
      <img src="./img/strakova.jpg" class="medialonek_photo"/>
    
      <div class="medialonek_cv">Vystudovala právnickou fakultu  na Masarykově univerzitě v Brně, kde napsala   diplomku na téma "Licence  Creative Commons v českém právním řádu".   Věnuje se také otevřeným datům a  aktuálně pracuje jako manažerka Open  Access na rektorátu Masarykovy univerzity. Dále působí jako  dobrovolnice  v organizaci Creative Commons v ČR.</div>
    </div>

    <div class="img-thumbnail medialonek">
      <div class="medialonek_name">Michal Černý</div>
      <img src="./img/cerny.jpg" class="medialonek_photo"/>
    
      <div class="medialonek_cv">
      Michal je garantem oborových didaktik na KISK FF MU, kde také vyučuje
předměty související s technologiemi ve vzdělávání a učící se společností.
Systematicky se věnuje pronikání technologií do škol, dostupnosti
softwaru, studijních materiálů a otevřenosti vědeckých dat. Studuje
doktorský studijní program, tématem jeho disertační práce jsou filosofické
otázky fyziky.
      </div>
    </div>

    <div class="img-thumbnail medialonek">
      <div class="medialonek_name">Libor Štěpánek</div>
      <img src="./img/stepanek.jpg" class="medialonek_photo"/>
    
      <div class="medialonek_cv">
      Libor je vysokoškolský učitel angličtiny, historik, politolog a budoucí ředitel Centra jazykového vzdělávání MU.</div>
    </div>

    <div class="img-thumbnail medialonek">
      <div class="medialonek_name">Tomáš Raděj</div>
      <img src="./img/radej.jpg" class="medialonek_photo"/>
    
      <div class="medialonek_cv">Tomáš absolvoval Fakultu informatiky Masarykovy univerzity v Brně diplomovou prací "Nástroj pro analýzu softwarových licencí". Momentálně pracuje ve společnosti Red Hat jako vývojář v Pythonu a Fedoře, amatérsky se věnuje Free/Open Source Softwaru, copyrightu a licencování. Můžete se s ním setkat na brněnských i pražských FOSS konferencích, včetně OpenAltu. <br/> Tomáš bude panelovou diskusi moderovat.</div>
    </div>
   

<div style="clear: both">&nbsp;</div>



<p>Pro pokládání dotazů panelistům lze použít následující:
<ul>
  <li>
    Vlastní ústa při osobní účasti
  </li>
  <li>
    Konferenční chat klient na této stránce vpravo dole
  </li>
  <li>
    <a href="https://diasp.eu/u/openalt">Diaspora</a> (přidat komentář)
  </li>
  <li>
    <a href="https://twitter.com/openalt">Twitter</a> (hashtag #OA_AaronS)
  </li>
  <li>
    <a href="https://www.facebook.com/events/324169457773166/">Facebook</a> (přidat komentář)
  </li>
  <li>
    <a href="https://plus.google.com/+OpenAlt/posts">G+</a> (přidat komentář)
  </li>
  <li>
    Email <a href="mailto:diskuse@openalt.org">diskuse@openalt.org</a>
  </li>
</ul>
</p>

  </div>
</section -->


<section class="section-normal section-link" id="venue">
  <div class="container">
    <h1> Místo konání </h1>
    <p>
EOF;

  $content .= $config['venue_text'];
  $venue_coord = $config['venue_coord'];

$content .= <<<EOF
    </p>
  </div>
</section>

<div id="map" style="width: 100%; height: 300px; border: 0; margin: 0;" > </div>

 <script src="./js/leaflet.js"></script>
<script type="text/javascript">
    map = new L.Map('map');
    var osmUrl='//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {minZoom:4, maxZoom: 18, attribution: osmAttrib});

    // start the map in South-East England
    map.setView([$venue_coord], 15);
    map.addLayer(osm);
    var marker = L.marker([$venue_coord]).addTo(map);
    map.dragging.disable();
    map.touchZoom.disable();
    map.doubleClickZoom.disable();
    map.scrollWheelZoom.disable();

</script>

EOF;



$content .= <<<EOF

<section class="section-normal section-link" id="contact">
  <div class="container">
    <h1> Kontakt </h1>

Náš spolek pořádá každý třetí pátek v měsící srazy zaměřené na open-source, open-hardware, open-mind, otevřenou kulturu, politku a vůbec další věci.
Součástí srazů je popíjení různých lahodných nápojů, které podporují konverzaci. Rádi bychom naše myšlenky předali i dalším lidem a obohatili je i
o další nové vědomosti. Srazy jsou proto určeny pro širokou veřejnost. <br/> <br/>

<address>
  <strong>OpenAlt z.s.</strong><br/>
  registrováno MVČR dne 18.11.2010 pod č.j. VS-1-1/82038/10-R, IČO: 22848037 <br/>
  Sídlo: Lamačova 634/2, Praha 52, 152 00 
</address>
  <a href="http://www.openalt.org">http://www.openalt.org</a>
<p>
Navštivte nás na naší Jabber konferenci: <a href="xmpp:#">conf@openalt.org</a>
</p>


<a href="https://www.facebook.com/OpenAlt" role="button"><img src="./img/icons/facebook.png" class="img-rounded" alt="facebook" /></a>
<a href="https://twitter.com/OpenAlt" role="button"><img src="./img/icons/twitter.png" class="img-rounded" alt="twitter" /></a> 
<a href="https://plus.google.com/+OpenAlt" role="button"><img src="./img/icons/google-plus.png" class="img-rounded" alt="google plus" /> </a>
<a href="https://linkedin.com/company/openalt-z.s." role="button"><img src="./img/icons/linkedin.png" class="img-rounded" alt="linked in" /> </a>
<a href="https://openalt.org/feed/" role="button"><img src="./img/icons/rss.png" class="img-rounded" alt="rss"/> </a>

<a href="http://cz.redhat.com/"><img src="./img/RHLogo.jpg" style="height: 60px; margin: 20px;"></a>
<a href="http://www.pirati.cz/"><img src="./img/pirati.svg" style="height: 60px; margin: 20px;"></a>
<a href="http://su.fi.muni.cz/"><img src="./img/sufi.png" style="height: 40px; margin: 20px;"></a>
<a href="http://www.studentsforlibertycz.cz/"><img src="./img/SFF.png" style="height: 60px; margin: 20px; "></a>
<a href="http://www.kinoscala.cz/"><img src="./img/MU-Scala.jpg" style="height: 60px; margin: 20px; "></a>
<a href="http://www.wikimedia.cz/"><img src="./img/600px-Wikimedia_Czech_Republic-logo.svg.png" style="height: 60px; margin: 20px; "></a>
<br/>
<a href="./img/Noc%20Otevreneho%20Softwaru%20v%20Umeni%20PRINTED.pdf">plakát</a>

  </div>
</section>

EOF;


require(dirname(__FILE__).'/template.php');
?>
