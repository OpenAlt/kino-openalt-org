##!/usr/bin/env bash

set -e
set -o pipefail

cp -a "$1/." "$2"

cd "$2"

for phpFile in $(find . -type f -name '*.php'); do
  htmlFile="${phpFile%'.php'}.html"
  echo "build ${htmlFile} from ${phpFile}"
  php -f "${phpFile}" > "${htmlFile}"
done

for phpFile in $(find . -type f -name '*.php'); do
  htmlFile="${phpFile%'.php'}.html"
  echo "copy ${htmlFile} over ${phpFile}"
  cp "${htmlFile}" "${phpFile}"
done
