# Kino.OpenAlt.org

Repositář pro web Kino.OpenAlt.org. Sestavení probíhá s využitím [GitLab CI/CD](https://docs.gitlab.com/ce/ci/).

## Úpravy obsahu

### Šablona a obsah
Soubory tvořící celý web se nachází v adresáři `src`. Soubory `.php` se pomocí PHP CLI převádí na HTML.

### Příprava
Abyste byli schopni spustit níže uvedené příkazy, je nutné mít nainstalovaný _make_ a [Docker](https://docs.docker.com/get-docker/) nebo [Podman](https://podman.io/getting-started/installation).

## Sestavení statické verze
Pokud používáte Docker, pro sestavení můžete použít tento příkaz.
```
$ make build_in_docker
```
Na systémech, kde Docker nefunguje, nebo pokud upřednostňujete alternativní Podman, použijte tento příkaz.
```
$ make build_in_podman
```
Statická verze je vygenerovaná do adresáře `html`. Pro nasazení stačí jeho obsah nahrát na server třeba přes FTP.

## Použití jako dynamického PHP
Místo sestavování statické verze můžete použít webserver s PHP a obsah adresáře `src` nahrát přímo do web rootu. To byl také původní způsob provozu webu.
