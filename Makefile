CONTAINER_IMAGE=docker.io/php:8.1-cli-bullseye

build:
	make clean
	bash ./build-php-to-html.sh ${PWD}/src ${PWD}/html

build_in_docker:
	docker pull $(CONTAINER_IMAGE)
	docker run \
		--workdir $(PWD) \
		-v $(PWD):$(PWD) \
		--rm=true \
		--entrypoint=/bin/bash \
		$(CONTAINER_IMAGE) -c make build

build_in_podman:
	podman pull $(CONTAINER_IMAGE)
	podman run \
		--workdir $(PWD) \
		-v $(PWD):$(PWD):Z \
		--rm=true \
		--entrypoint=/bin/bash \
		$(CONTAINER_IMAGE) -c make build

clean:
	find html -mindepth 1 -maxdepth 1 -not -name '.keep' -exec rm -rf '{}' \;
